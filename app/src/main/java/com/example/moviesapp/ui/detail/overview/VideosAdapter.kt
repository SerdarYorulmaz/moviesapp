package com.example.moviesapp.ui.detail.overview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.Constraints
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesapp.R
import com.example.moviesapp.binding.ImageBindingAdapter

import com.example.moviesapp.model.videos.MovieVideoResults
import com.example.moviesapp.util.Constants
import com.example.moviesapp.util.UniversalImageLoader
import kotlinx.android.synthetic.main.item_video.view.*

class VideosAdapter(private val interactions: InteractionVideo) : ListAdapter<MovieVideoResults, VideosAdapter.ViewHolder>(MoviesVideoDC()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_video, parent, false),interactions)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
    fun swapData(data: List<MovieVideoResults>) {
        submitList(data.toMutableList())
    }

    inner class ViewHolder(itemView: View,private val interactions: InteractionVideo) : RecyclerView.ViewHolder(itemView),View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val clicked = getItem(adapterPosition)
            interactions.commentClicked(clicked)
        }

        fun bind(movieVideoResults: MovieVideoResults) = with(itemView) {

            ImageBindingAdapter.imgLoading(Constants.IMAGE_W342, movieVideoResults.site, item_video_thumbnail)
            item_video_title.setText(movieVideoResults.name)
        }
    }

    interface InteractionVideo {
        fun commentClicked(model: MovieVideoResults)
    }

    private class MoviesVideoDC : DiffUtil.ItemCallback<MovieVideoResults>() {


        override fun areItemsTheSame(
            oldItem: MovieVideoResults,
            newItem: MovieVideoResults
        ): Boolean =
            oldItem.id == newItem.id


        /** objeleri karsilastirir   aynı mı değimi diye recyclerview degisklikten haberdar ediyor*/

        override fun areContentsTheSame(
            oldItem: MovieVideoResults,
            newItem: MovieVideoResults
        ): Boolean =
            oldItem.name == newItem.name


        /** icerikleri karsilastirir aynı mı değimi diye recyclerview degisklikten haberdar ediyor */
    }
}