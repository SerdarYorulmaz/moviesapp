package com.example.moviesapp.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.moviesapp.data.remote.ApiClient
import com.example.moviesapp.data.remote.ApiService
import com.example.moviesapp.model.movies.MovieResponse
import com.example.moviesapp.model.movies.MovieResults
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainRepository {

    /**  lazy ile tek satirda yapiyorz normalde init tanimlayip esitlemek geerkecekti*/

    private val apiService: ApiService by lazy { ApiClient.getApiService() }


    fun getPopularMovies(): LiveData<List<MovieResults>>? {

        val moviesLiveData: MutableLiveData<List<MovieResults>> = MutableLiveData()

        apiService.getPopularMovies().enqueue(object : Callback<MovieResponse> {
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {

                /** hatayi log olarak gosteriyor */
                Log.e("getPopularMovies", t.message)
            }

            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {

                /** Burdada serviste gelen response model aktırdıgından modeldeki veriyi livedata vaolu oarak aktsriyoruz */

                moviesLiveData.value = response.body()?.results
            }

        })

        return moviesLiveData

    }

    fun getTopRatedMovies(): LiveData<List<MovieResults>>? {

        val moviesRatedLiveData: MutableLiveData<List<MovieResults>> = MutableLiveData()

        apiService.getTopRatedMovies().enqueue(object :Callback<MovieResponse>{
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {

                Log.e("getTopRatedMovies",t.message)
            }

            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {

                moviesRatedLiveData.value=response.body()?.results
            }

        })
        return  moviesRatedLiveData

    }
}