package com.example.moviesapp.ui.main

import android.content.Intent

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesapp.R
import com.example.moviesapp.binding.ImageBindingAdapter


import com.example.moviesapp.model.movies.MovieResponse
import com.example.moviesapp.model.movies.MovieResults
import com.example.moviesapp.util.Constants
import com.example.moviesapp.util.UniversalImageLoader
import kotlinx.android.synthetic.main.item_movie.view.*

class MovieAdapter(private val interaction: Interaction) :
    ListAdapter<MovieResults, MovieAdapter.ViewHolder>(MoviesDC()) {
    private  lateinit var img:ImageView
    /** ListAdapter ,recyclerview bir extension ı */


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false),
            interaction
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(getItem(position))
    }

    fun swapData(data: List<MovieResults>) {
        submitList(data.toMutableList())
    }


    inner class ViewHolder(itemView: View, private val interaction: Interaction) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val clicked = getItem(adapterPosition)
            interaction.commentClicked(clicked)
        }


//        fun create(inflater: LayoutInflater, parent: ViewGroup): ViewHolder {
//           val itemMovieBinding = ItemMovieBinding.inflate(inflater, parent, false)
//           return ViewHolder(itemMovieBinding)
//       }


        fun bind(movieResults: MovieResults) = with(itemView) {
            var result=findViewById<ImageView>(R.id.item_movie_poster)
            var temp:String= Constants.IMAGE_BASE_URL+ Constants.IMAGE_W342+movieResults.posterPath
            UniversalImageLoader.setImage(temp,result,null)
            item_movie_title.setText(movieResults.title)



        }


    }

    interface Interaction {
        fun commentClicked(model: MovieResults)
    }

    private class MoviesDC : DiffUtil.ItemCallback<MovieResults>() {


        override fun areItemsTheSame(oldItem: MovieResults, newItem: MovieResults): Boolean =
            oldItem.movieId == newItem.movieId

        /** objeleri karsilastirir   aynı mı değimi diye recyclerview degisklikten haberdar ediyor*/

        override fun areContentsTheSame(oldItem: MovieResults, newItem: MovieResults): Boolean =
            oldItem.title == newItem.title

        /** icerikleri karsilastirir aynı mı değimi diye recyclerview degisklikten haberdar ediyor */
    }


}