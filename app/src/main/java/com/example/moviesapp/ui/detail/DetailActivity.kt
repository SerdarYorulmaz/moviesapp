package com.example.moviesapp.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.moviesapp.R
import com.example.moviesapp.binding.ImageBindingAdapter
import com.example.moviesapp.common.BaseActivity
import com.example.moviesapp.common.BaseVMActivity
import com.example.moviesapp.common.ViewPagerAdapter
import com.example.moviesapp.databinding.ActivityDetailBinding
import com.example.moviesapp.model.movies.MovieResults
import com.example.moviesapp.ui.detail.overview.OverviewFragment
import com.example.moviesapp.util.Constants
import com.example.moviesapp.util.UniversalImageLoader
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : BaseVMActivity<DetailViewModel>() {

    private var movie: MovieResults? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)  //databing kullaiyoruz buna gerek yok

        //detail_movie_name text olan
        //movie_detail_poster_path image olan
        //detail_backdrop
        setupUI()

        /** Fragmentle gonderdigimiz verileri simdi alalım */

        intent.extras.let {
            movie = it?.getParcelable(Constants.EXTRA_POPULAR_MOVIES)

            setupViewPager(movie)
            fabBehaviour(movie)
            detail_tabs.setupWithViewPager(detail_viewpager) //viewpager da verdik

            ImageBindingAdapter.imgLoading(Constants.IMAGE_W342,movie?.posterPath,movie_detail_poster_path)
            detail_movie_name.setText(movie?.title)
            ImageBindingAdapter.imgLoading(Constants.IMAGE_W500,movie?.backdropPath,detail_backdrop)


//            fabBehaviour(movie)
//            detail_tabs.setupWithViewPager(detail_viewpager)

            // dataBinding.movie = movie
            /** diger activtyden gelen degerileri verdik */
        }
        Toast.makeText(
            this,
            "secilen flim idsi:${(movie?.movieId).toString()}  ve title:${(movie?.title)} ",
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun setupUI() {
        setSupportActionBar(detail_toolbar)
        /** bir onceki activity gecis yapmasini sayliyor toolbar usteki geri button */

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        /** toolbarda title gostermiyoruz */

    }


    private fun setupViewPager(movie: MovieResults?) {
        /** datail activtiydeki fragmentlere viegmager yerlestirilmesini koaylastiriyor */
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.apply {
            addFragment(OverviewFragment.newInstance(movie),"Overview")
        }

        detail_viewpager.adapter = adapter

    }

    /** scroll yaptigimizda floating action bar kaybollmuyor amacımız appbar layout en uste oldugu zaman floating action bari gizleyecegiz
     * diger durumdada ise gosterecegiz*/

    private fun fabBehaviour(movie: MovieResults?) {
        detail_appbar_layout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener {
                appBarLayout, verticalOffset ->
            if(Math.abs(verticalOffset)-appBarLayout.totalScrollRange==0){
                favorite_fab.hide() //totalScrollRange==0 ise actionbar en üstedir onun icin gizliyoruz.

                supportActionBar?.setDisplayShowTitleEnabled(true) // actionbar enuste geldiginde  toolbarda title gozuksun
                detail_toolbar.title=movie?.title

            }else{
                /** bu durumdada actionbar en uste degi ldir */
                favorite_fab.show()
                supportActionBar?.setDisplayShowTitleEnabled(false)
                detail_toolbar.title=" "
            }
        })
        /** collapsing_toolbarlayout rengi  */
        detail_collapsing_toolbarlayout.setExpandedTitleColor(resources.getColor(android.R.color.transparent))
    }


    override fun getViewModel(): Class<DetailViewModel> {
        return DetailViewModel::class.java
    }
}
