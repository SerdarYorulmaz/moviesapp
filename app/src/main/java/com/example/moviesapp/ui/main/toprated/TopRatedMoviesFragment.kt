package com.example.moviesapp.ui.main.toprated

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager

import com.example.moviesapp.R
import com.example.moviesapp.common.BaseVMFragment
import com.example.moviesapp.model.movies.MovieResults
import com.example.moviesapp.ui.main.MovieAdapter
import com.example.moviesapp.util.gone
import com.example.moviesapp.util.visible
import kotlinx.android.synthetic.main.fragment_top_rated_movies.*


class TopRatedMoviesFragment : BaseVMFragment<TopRatedMoviesViewModel>(),MovieAdapter.Interaction {

    private lateinit var adapter:MovieAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_top_rated_movies, container, false)
    }

    override fun getViewModel(): Class<TopRatedMoviesViewModel> {
       return TopRatedMoviesViewModel::class.java
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val temp: MovieAdapter by lazy { MovieAdapter(this) }
        adapter = temp


        rated_recyclerview.layoutManager=GridLayoutManager(activity,2)

        viewModel.getTopRatedMovies()?.observe(viewLifecycleOwner, Observer{

            adapter.submitList(it) /** listede bir degisiklik oldunda update oldugunda haber ver  recyclera*/

            rated_recyclerview.adapter=adapter
            rated_recyclerview.visible()
            rated_progressbar.gone()
        })

    }

    override fun commentClicked(model: MovieResults) {
        Toast.makeText(activity,"TiKladin", Toast.LENGTH_SHORT).show()
    }

}
