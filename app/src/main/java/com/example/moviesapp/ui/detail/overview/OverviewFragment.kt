package com.example.moviesapp.ui.detail.overview

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer

import com.example.moviesapp.R
import com.example.moviesapp.common.BaseVMFragment
import com.example.moviesapp.model.movies.MovieResults
import com.example.moviesapp.model.videos.MovieVideoResults
import com.example.moviesapp.util.Constants
import com.example.moviesapp.util.gone
import com.example.moviesapp.util.visible
import kotlinx.android.synthetic.main.fragment_overview.*
import kotlinx.android.synthetic.main.fragment_overview.view.*


class OverviewFragment : BaseVMFragment<OverviewViewModel>(),VideosAdapter.InteractionVideo {

    private lateinit var videosAdapter: VideosAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view= inflater.inflate(R.layout.fragment_overview, container, false)

        return view;
    }

    /** viewmodel da verdik */
    override fun getViewModel(): Class<OverviewViewModel> =OverviewViewModel::class.java


    companion object{
        /**  activityden veriler gondermek icin  */
        private  const val MOVIE_KEY="movie_overview_key"

        fun newInstance(movie:MovieResults?):OverviewFragment{
            val args=Bundle()
            args.putParcelable(MOVIE_KEY,movie)

            val fragment=OverviewFragment()
            fragment.arguments=args
            return fragment
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        /**activty yaratıldıktan sonra bu islemleri yap */


        val movie=arguments?.getParcelable<MovieResults>(MOVIE_KEY) as MovieResults

        val movieId=movie.movieId


        viewModel.getDetails(movieId).observe(viewLifecycleOwner, Observer {
            /** degisklikleri ui aatiyoruz */
            release_date.setText(it.releaseDate)
            overview.setText(it.overview)

        })

        /** ilk olarak progress bar gosterilecek */
        detail_movie_videos_progress.visible()
        movie_videos_recyclerview.gone()

        viewModel.getMovieVideos(movieId).observe(viewLifecycleOwner, Observer {

            val temp: VideosAdapter by lazy { VideosAdapter(this) }
            videosAdapter = temp


            movie_videos_recyclerview.adapter=videosAdapter
            videosAdapter.submitList(it)

            detail_movie_videos_progress.gone()
            movie_videos_recyclerview.visible()

        })

    }

    override fun commentClicked(model: MovieVideoResults) {
        /** video tiklanidiginda videonun youtube acilmasini istiyoruz */
        val intent=Intent(Intent.ACTION_VIEW)
        intent.data= Uri.parse(Constants.YOUTUBE_WATCH_URL+model.key)
        startActivity(intent)
    }


    /**
 * release_date text olan
 * overview text olan aciklama
     * overview
 * */
}
