package com.example.moviesapp.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.example.moviesapp.R
import com.example.moviesapp.common.ViewPagerAdapter
import com.example.moviesapp.ui.main.popular.PopularMoviesFragment
import com.example.moviesapp.ui.main.toprated.TopRatedMoviesFragment
import com.example.moviesapp.util.UniversalImageLoader
import com.nostra13.universalimageloader.core.ImageLoader
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        setupUI()
    }

    private fun initImageLoader() {

        var universalImageLoader = UniversalImageLoader(this)
        ImageLoader.getInstance().init(universalImageLoader.config)

    }

    private  fun setupUI(){
        setSupportActionBar(main_toolbar)
        setupViewPager()
        main_tabs.setupWithViewPager(main_viewpager)
        initImageLoader()
    }

    private fun setupViewPager(){
        val adapter=ViewPagerAdapter(supportFragmentManager)
        adapter.apply {
            addFragment(PopularMoviesFragment(),"Popular")
            addFragment(TopRatedMoviesFragment(),"Top Rated")
        }

        main_viewpager.adapter=adapter
    }
}


/** themoviedb api key
 *
 * 6e63c2317fbe963d76c3bdc2b785f6d1
 * 36cb137632f543a36ecec5e72429a02d
 * */