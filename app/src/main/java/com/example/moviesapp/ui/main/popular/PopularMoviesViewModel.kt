package com.example.moviesapp.ui.main.popular

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.moviesapp.model.movies.MovieResults
import com.example.moviesapp.ui.main.MainRepository

class PopularMoviesViewModel:ViewModel() {

    /** respository cagirip sonra getPopularMovies cagirmak almk gerekiyor */

    private val repository:MainRepository by lazy { MainRepository() }

    fun getPopularMovies():LiveData<List<MovieResults>>?=repository.getPopularMovies()


}