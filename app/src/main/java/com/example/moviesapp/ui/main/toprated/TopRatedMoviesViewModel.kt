package com.example.moviesapp.ui.main.toprated

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.moviesapp.model.movies.MovieResults
import com.example.moviesapp.ui.main.MainRepository

class TopRatedMoviesViewModel : ViewModel() {

    private val repository: MainRepository by lazy { MainRepository() }

    fun getTopRatedMovies(): LiveData<List<MovieResults>>? {
        return repository.getTopRatedMovies()
    }
}