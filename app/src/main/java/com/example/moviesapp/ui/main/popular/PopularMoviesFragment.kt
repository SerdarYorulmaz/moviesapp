package com.example.moviesapp.ui.main.popular

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager

import com.example.moviesapp.R
import com.example.moviesapp.common.BaseVMFragment
import com.example.moviesapp.model.movies.MovieResults
import com.example.moviesapp.ui.detail.DetailActivity
import com.example.moviesapp.ui.main.MovieAdapter
import com.example.moviesapp.util.Constants
import com.example.moviesapp.util.gone
import com.example.moviesapp.util.visible
import kotlinx.android.synthetic.main.fragment_popular_movies.*


class PopularMoviesFragment : BaseVMFragment<PopularMoviesViewModel>(),MovieAdapter.Interaction{

    private lateinit var adapter:MovieAdapter

    /** Fragment biz base vermistik suan databing kullanmayacagiz onun icinde BaseVMFragment kalıtcaz*/

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_popular_movies, container, false)
    }

    override fun getViewModel(): Class<PopularMoviesViewModel> {
       return  PopularMoviesViewModel::class.java
    }

    /** view tanimla islemini findviewid seklinde yaiyorduk  id direk olarak aldigimiz icin  daha view  olusturulmadigi icin hata alirix onıun
     * icin onViewCreatedtanimliyoruz */

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        /** burda viewmodel BaseVMFragment geliyor */
        val temp: MovieAdapter by lazy { MovieAdapter(this) }
        adapter = temp
        //TODO this yerine viewLifecycleOwner yaptik onviewCretated  boyle oluyor



      //test
        popular_recyclerview.layoutManager=GridLayoutManager(activity,2)

        viewModel.getPopularMovies()?.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)  /** updatelist sekilini submitlist seklinde yazilmis. adapter bir guncellme oldugunda bu calisicacak*/
            popular_recyclerview.adapter=adapter

            popular_recyclerview.visible()
            popular_progressbar.gone()
        })



    }

    override fun commentClicked(model: MovieResults) {
        //Toast.makeText(activity,"TiKladin",Toast.LENGTH_SHORT).show()
        Toast.makeText(activity,"${model.title}",Toast.LENGTH_SHORT).show()
        val intent=Intent(activity,DetailActivity::class.java)
        intent.putExtra(Constants.EXTRA_POPULAR_MOVIES,model)
        startActivity(intent)
    }

//    override fun onItemClicked(movieResults: MovieResults) {
//        val intent=Intent(activity,DetailActivity::class.java)
//        intent.putExtra(Constants.EXTRA_POPULAR_MOVIES,movieResults)
//        startActivity(intent)
//    }


}
