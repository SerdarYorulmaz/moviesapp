package com.example.moviesapp.util

import android.view.View
import com.example.moviesapp.ui.main.MovieAdapter


fun View.visible(){
    this.visibility=View.VISIBLE
}

fun View.gone(){
    this.visibility=View.GONE
}