package com.example.moviesapp.data.remote

import com.example.moviesapp.util.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {

    /** normalde OkHttp retrofit var aa biz api key devamli query yazacagimiza onu interceptor sayesin otomize ettik */

    fun getApiService(): ApiService {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(getOkHttpClient())

            .build()

        return retrofit.create(ApiService::class.java)
    }

    /** asagidai functionda  olusturdugmuz intercepter verecegiz */

    private fun getOkHttpClient(): OkHttpClient {
        val client = OkHttpClient.Builder()
        client.addInterceptor(RequestInterceptor())
        return client.build()
    }
}