package com.example.moviesapp.data.remote

import com.example.moviesapp.util.Constants
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class RequestInterceptor : Interceptor {


    /** biz her istek attigimizda  url sonuna  API keyi ekleticez interceptor yapmasaydik yine path seklinde eklmek zorunda kalirdik
     * api keyi biz query olarak almadik onun yerine interceptor kullanacagiz.
     *
     *
     * */

    override fun intercept(chain: Interceptor.Chain): Response {

        /** //api.themoviedb.org/3/movie/157336/reviews? */
        val originalRequest: Request = chain.request()


        /**http eklenmis hali -->  https://api.themoviedb.org/3/movie/157336/reviews? */
        val originalHttpUrl: HttpUrl = originalRequest.url



        /**  url mize en son olarak api_key yani Query parameteresi ekleyecegiz.  */

        val url = originalHttpUrl.newBuilder()
            .addQueryParameter("api_key", Constants.API_KEY)
            .build()

        val request = originalRequest.newBuilder().url(url).build()
        return chain.proceed(request)

    }

}