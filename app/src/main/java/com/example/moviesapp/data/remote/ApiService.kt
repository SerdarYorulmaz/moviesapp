package com.example.moviesapp.data.remote

import com.example.moviesapp.model.detail.MovieDetailResponse
import com.example.moviesapp.model.movies.MovieResponse
import com.example.moviesapp.model.reviews.MovieVideoReviewsResponse
import com.example.moviesapp.model.videos.MovieVideoResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiService {

    /** API servislerimizzi yazdik */

   /** popular movies */

    @GET("movie/popular")
    fun getPopularMovies():Call<MovieResponse>

    /** Top rated movies */

    @GET("movie/top_rated")
    fun getTopRatedMovies():Call<MovieResponse>

    /** Movie details */

    @GET("movie/{id}")
    fun getMovieDetails(@Path("id") movieId:Int):Call<MovieDetailResponse>

    /** Movie videos */

    @GET("movie/{id}/videos")
    fun getMovieVideos(@Path("id")movieId: Int):Call<MovieVideoResponse>

    /** Movie reviews */

    @GET("movie/{id}/reviews")
    fun getMovieReviews(@Path("id")movieId: Int):Call<MovieVideoReviewsResponse>

}