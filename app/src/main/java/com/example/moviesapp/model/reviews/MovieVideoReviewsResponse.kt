package com.example.moviesapp.model.reviews

import com.google.gson.annotations.SerializedName

data class MovieVideoReviewsResponse(

    @SerializedName("id")
    var id:Int,

    @SerializedName("results")
    var results:List<MovieVideoReviewsResults>
) {
}