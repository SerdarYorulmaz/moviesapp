package com.example.moviesapp.model.movies

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieResults(


    /** Result Kısmı  (liste olan)*/

    @SerializedName("id")
    var movieId:Int,

    @SerializedName("poster_path")
    var posterPath:String,

    @SerializedName("overview")
    var overview:String,

    @SerializedName("release_date")
    var releaseDate:String,

    @SerializedName("original_title")
    var orginalTitle:String,

    @SerializedName("title")
    var title:String,

    @SerializedName("backdrop_path")
    var backdropPath:String,

    @SerializedName("popularity")
    var popularity:Double,

    @SerializedName("vote_count")
    var voteCount:Int,

    @SerializedName("video")
    var video:Boolean,

    @SerializedName("vote_average")
    var voteAverage:Double


): Parcelable