package com.example.moviesapp.binding

import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.example.moviesapp.util.Constants
import com.example.moviesapp.util.UniversalImageLoader

object ImageBindingAdapter {

    /** @JvmStatic yazmamisin sebebi classlar compiler edilirken bu methodun static olmasi icin JVM icin  */

    @BindingAdapter("imageUrl")
    @JvmStatic
    fun loadImage(imageView: ImageView, url: String): ImageView? {

        /** url =kqil17.jpg kismi */

        if (url.isNotEmpty()) {
            Glide.with(imageView.context)
                .load(Constants.IMAGE_BASE_URL + Constants.IMAGE_W342 + url)
                .into(imageView)
            return imageView
        }

        return null

    }

    @JvmStatic
    fun imgLoading(IMAGE_W342: String, posterPath: String?, img: ImageView) {
        if (posterPath != null) {
            var temp: String = Constants.IMAGE_BASE_URL + IMAGE_W342 + posterPath
            UniversalImageLoader.setImage(temp, img, null)

        } else Log.e("movieImage", "movieImage Error")
    }

}