package com.example.moviesapp.common

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders

abstract class BaseActivity <DB:ViewDataBinding,VM:ViewModel>:AppCompatActivity() {

    /**
     * Asıl Amac kod tekrar olmaması icin view model,databing islemlerini base cclass aliyoruz.
     * biz projede databind ve viewmodel lari kullanacagiz.  view model late init var view modell veriler olusturuyorduk.
     * databinde setContex tanimliyorduk bunlar tek bir class toplayip  yavru class kalıtalım.
     * Bu class dedigimi gibi hem databing hem view model odlugunu dusunuyoruz
     * AppCompatActivity() turetiyoruz cunku activitlere bu AppCompatActivity() kalıtılır.
     *
     * Layuot lari vermesi icin getLayoutRes() method olusturyoruz(Bu methodun amaci R.layout.activity..(layout id sini almak) gibi idleri cagirmak)
     *
     *
     * */

    lateinit var dataBinding: DB
    lateinit var viewModel: VM

    @LayoutRes
    abstract fun getLayoutRes():Int   /** bu method bize R.layout.activity_main gibi islemlerde ise yarayacak @Layout dosyasi oldugunu bidirmek icin LayoutRes kullailir.*/

    abstract fun getViewModel():Class<VM>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /** databind tanimliyoruz */

        dataBinding=DataBindingUtil.setContentView(this,getLayoutRes()) /** alt class larda getlayoutres=R.layout.activiy gibi yaptigimzda direk base class gelecek */
        viewModel=ViewModelProviders.of(this).get(getViewModel())
    }



}