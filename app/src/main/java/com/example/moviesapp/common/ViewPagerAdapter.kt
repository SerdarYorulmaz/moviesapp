package com.example.moviesapp.common

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class ViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    /** fragmentlarin ve onlarin title larinin listesini aldik */
    private val fragmentList = ArrayList<Fragment>()
    private val fragmentTitleList = ArrayList<String>()

    /**get item ile o anki fragment aliyoruz */
    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    /**getCount  fragmentin size aliyoruz */
    override fun getCount(): Int {
        return fragmentList.size
    }

    /** FragmentTitele listeki  o anki fragment title aliyoruz*/
    override fun getPageTitle(position: Int): CharSequence? {

        return fragmentTitleList[position]
    }

    fun addFragment(fragment: Fragment, title: String) {
        fragmentList.add(fragment)
        fragmentTitleList.add(title)
    }
}