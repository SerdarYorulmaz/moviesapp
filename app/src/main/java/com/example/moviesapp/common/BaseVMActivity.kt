package com.example.moviesapp.common

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders

abstract class BaseVMActivity <VM: ViewModel>: AppCompatActivity() {

    /**  sadece viewmodel sahip olan classlar icin.
     *Projeizde butun classlari databing ile yazmayacagiz.Bazi classlarda databing kullanmak gerksiz olacak.
     *  Örneğin sadece vm ihtiyaci olacak o class.
     *
     * */


    lateinit var viewModel: VM


    abstract fun getViewModel():Class<VM>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /** databind tanimliyoruz */


        viewModel= ViewModelProviders.of(this).get(getViewModel())
    }



}